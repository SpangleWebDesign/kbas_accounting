document.addEventListener('DOMContentLoaded', function() {
	let burgerEl = document.querySelector(".burger-icon");
	let topBarEl = document.querySelector(".js_top");
	let middleBarEl = document.querySelector(".js_middle");
	let bottomBarEl = document.querySelector(".js_bottom");
	let selected = false;

	// Burger button animation
	burgerEl.addEventListener('click', function(event) {
		if(!selected) {
		  topBarEl.classList.add("top");
		  middleBarEl.classList.add("bottom");
		  bottomBarEl.classList.add("bottom");
		  // hiddenNavEl.classList.add("expand");
		} else {
		  topBarEl.classList.remove("top");
		  middleBarEl.classList.remove("bottom");
		  bottomBarEl.classList.remove("bottom");
		}
		selected = !selected;
	});
});